import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  celsius: number;
  fheit: number;
  


  constructor() {}

  ngOnInit() {
    this.celsius=0;
    this.fheit= this.celsius * 9 / 5 +32;
  }

  celsius_changed() {
    this.fheit = this.celsius * 9 / 5 + 32;
  
  }

  fheit_changed() {
    this.celsius = (this.fheit - 32) * 5 /9;
  }


}
